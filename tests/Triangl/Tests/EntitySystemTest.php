<?php

namespace Triangl\Tests;

use Triangl\WebTestCase;
use Triangl\EntitySystemApplication;

/**
 * Functional test for Triangl Entity System.
 */
class EntitySystemTest extends WebTestCase {
    /**
     * Implemented.
     */
    public function createApplication() {
        return new EntitySystemApplication( 
            __DIR__ . "/../../../var", 
            array("test" => true) 
        );
    }
}

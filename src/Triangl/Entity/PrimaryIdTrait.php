<?php

namespace Triangl\Entity;

/**
 * Entity with id column as primary key.
 */
trait PrimaryIdTrait {
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    
    /**
     * Gets the id.
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}

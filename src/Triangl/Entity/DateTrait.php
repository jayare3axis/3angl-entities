<?php

namespace Triangl\Entity;

/**
 * Entity with date property.
 */
trait DateTrait {
    /** @Column(type="date") **/
    protected $date;
    
    /**
     * Gets the name.
     * @return string
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Sets the date.
     * @param string $date
     */
    public function setDate($date) {
        $this->date = $date;
    }
}

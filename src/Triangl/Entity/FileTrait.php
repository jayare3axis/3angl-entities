<?php

namespace Triangl\Entity;

/**
 * Entity with uploaded file.
 */
trait FileTrait {
    /** @Column(type="object") **/
    protected $file;
    
    /**
     * Gets the image.
     * @return Triangl\File\File
     */
    public function getFile() {
        return $this->file;
    }

    /**
     * Sets the image.
     * @param Triangl\File\File $file
     */
    public function setFile($file) {
        $this->file = $file;
    }
}

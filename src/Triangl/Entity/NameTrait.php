<?php

namespace Triangl\Entity;

/**
 * Entity with name property.
 */
trait NameTrait {
    /** @Column(type="string") **/
    protected $name;
    
    /**
     * Gets the name.
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Sets the name.
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }
    
    /**
     * Implemented.
     */
    public function __toString() {
        return $this->name;
    }
}

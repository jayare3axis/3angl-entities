<?php

namespace Triangl\Entity;

/**
 * Entity with order property.
 */
trait OrderTrait {
    /** @Column(type="smallint") **/
    protected $ord;
    
    /**
     * Gets the order.
     * @return int
     */
    public function getOrd() {
        return $this->ord;
    }

    /**
     * Sets the order.
     * @param int $ord
     */
    public function setOrd($ord) {
        $this->ord = $ord;
    }
}

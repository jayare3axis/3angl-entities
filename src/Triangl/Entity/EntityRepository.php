<?php

namespace Triangl\Entity;

use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * Base entity repository.
 */
class EntityRepository extends \Doctrine\ORM\EntityRepository {
    /**
     * Creates new record and returns it's primary key value.
     * @param array $values
     */
    public function create(array $values) {
        // Create instance.
        $em = $this->getEntityManager();
        $metaData = $em->getClassMetadata( $this->getEntityName() );
        $instance = $metaData->newInstance();
        
        // Set values.
        foreach ($values as $key => $val) {
            if ( !$this->isFileField($instance, $key, $val) ) {
                $this->onSetFieldValue($instance, $key, $val);
            }
        }
        
        // Persist.
        $em->persist($instance);
        $em->flush();
                
        return $metaData->getSingleIdReflectionProperty()->getValue($instance);
    }
    
    /**
     * Updates record.
     * @param mixed $id primary key
     * @param array $values
     */
    public function update($id, array $values) {
        $em = $this->getEntityManager();
        
        // Get instance.
        $instance = $this->find($id);
        
        // Set values.
        foreach ($values as $key => $val) {
            if ( !$this->isFileField($instance, $key, $val) ) {
                $this->onSetFieldValue($instance, $key, $val);
            }
        }
        
        // Persist.
        $em->persist($instance);
        $em->flush();
    }
    
    /**
     * Deletes record.
     * @param mixed $id primary key
     */
    public function delete($id) {
        $em = $this->getEntityManager();
        
        // Get instance.
        $instance = $this->find($id);
        
        // Remove.
        $em->remove($instance);
        $em->flush();
    }
    
    /**
     * Gets entities by given page.
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public function findPage($page, $pageSize) {
        // Create query.
        $qb = $this->getEntityManager()->createQueryBuilder();
        $query = $qb->select( array('o') )
                    ->from($this->getEntityName(), 'o');
        
        // Limit result.
        $limitedQuery = $query->setFirstResult($page * $pageSize)
                            ->setMaxResults($pageSize)
                            ->getQuery();
        
        // Get total count.
        $paginator = new Paginator($limitedQuery, $fetchJoinCollection = true);
        $c = count($paginator);
        
        return new PagedResult($limitedQuery->getResult(), $c);
    }
    
    /**
     * Invoked upon setting field value of entity.
     * @param mixed $instance
     * @param string $field
     * @param mixed $value
     */
    protected function onSetFieldValue($instance, $field, $value) {
        $em = $this->getEntityManager();
        $metaData = $em->getClassMetadata( $this->getEntityName() );
        $metaData->setFieldValue($instance, $field, $value);
    }
    
    /**
     * Determines if given field is file.
     * @param mixed $instance
     * @param string $field
     * @param mixed $value
     * @return boolean
     */
    protected function isFileField($instance, $field, $value) {
        // TO - DO implement better way
        return $field == "file";
    }
}

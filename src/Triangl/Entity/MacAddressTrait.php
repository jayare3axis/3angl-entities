<?php

namespace Triangl\Entity;

/**
 * Entity with mac address property.
 */
trait MacAddressTrait {
    /** @Column(type="string") **/
    protected $mac;
    
    /**
     * Gets the mac address.
     * @return string
     */
    public function getMac() {
        return $this->mac;
    }

    /**
     * Sets the order.
     * @param string $mac
     */
    public function setMac($mac) {
        $this->mac = $mac;
    }
}

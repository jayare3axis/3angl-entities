<?php

namespace Triangl\Entity;

use Triangl\Entity\NameTrait;

/**
 * Reprezents a file field.
 */
class FileField {
    use NameTrait;
    
    private $size;
    private $type;
    
    /**
     * Consctructor.
     */
    public function __construct($name, $size, $type) {
        $this->name = $name;
        $this->size = $size;
        $this->type = $type;
    }
    
    /**
     * Sets size.
     * @param int $size
     */
    public function setSize($size) {
        $this->size = $size;
    }
    
    /**
     * Gets size.
     * @return int
     */
    public function getSize() {
        return $this->size;
    }
    
    /**
     * Sets type.
     * @param string $type
     */
    public function setType($type) {
        $this->type = $type;
    }
    
    /**
     * Gets type.
     * @return string
     */
    public function getType() {
        return $this->type;
    }
    
    /**
     * Overriden.
     */
    public function __toString() {
        return $this->getName() . " <span style=\"opacity: 0.5;\">" . $this->formatBytes( $this->getSize() ) . "</span>";
    }
    
    /**
     * Helper method for formating file size.
     * @param int $bytes
     * @param int $precision
     * @return string
     */
    private function formatBytes($bytes, $precision = 2) { 
        $units = array('B', 'KB', 'MB', 'GB', 'TB'); 

        $bytes = max($bytes, 0); 
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
        $pow = min($pow, count($units) - 1); 

        // Uncomment one of the following alternatives
        // $bytes /= pow(1024, $pow);
        // $bytes /= (1 << (10 * $pow)); 

        return round($bytes, $precision) . ' ' . $units[$pow]; 
    } 
}

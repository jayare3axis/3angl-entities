<?php

namespace Triangl\Entity;

/**
 * Entity with alias property.
 */
trait AliasTrait {
    /** @Column(type="string") **/
    protected $alias;
    
    /**
     * Gets the alias.
     * @return string
     */
    public function getAlias() {
        return $this->alias;
    }

    /**
     * Sets the alias.
     * @param string $alias
     */
    public function setAlias($alias) {
        $this->alias = $alias;
    }
}

<?php

namespace Triangl\Entity;

/**
 * Query result with total count crate.
 */
class PagedResult {
    private $result;
    private $count;
    
    /**
     * Default constructor.
     */
    public function __construct(array $result, $count) {
        $this->result = $result;
        $this->count = $count;
    }
    
    /**
     * Gets the result.
     */
    public function getResult() {
        return $this->result;
    }
    
    /**
     * 
     */
    public function getCount() {
        return $this->count;
    }
}

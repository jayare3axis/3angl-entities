<?php

namespace Triangl;

/**
 * Helper routines for entity system module.
 */
class EntitySystemHelper {
    private $app;
    
    /**
     * Default constructor.
     */
    public function __construct(Application $app) {
        $this->app = $app;
    }
    
    /**
     * Registers new entity namespace for ORM.
     * @param array $entity options
     */
    public function addNamespace(array $entity) {
        $this->app['db.orm.entities'] = array_merge( $this->app['db.orm.entities'], array($entity) );
    }
    
    /**
     * Registers new entity filter.
     * @param string $name filter alias
     * @param string $className filter class
     */
    public function addFilter($name, $className) {
        $val = $this->app['db.orm.filters'];
        $val[$name] = $className;
        $this->app['db.orm.filters'] = $val;
    }
    
    /**
     * Gets list of entity properties by it's names or all properties.
     * @param string $className
     * @param array $properties
     */
    public function getProperties($className, $properties = null) {
        $metaData = $this->app['db.orm.em']->getClassMetadata($className);
        
        $result = array();
        if ($properties !== null) {            
            foreach ($properties as $property) {
                if ( strcmp( $property, $metaData->getSingleIdReflectionProperty()->getName() ) != 0 ) {
                    // Handle property access privilegies if any.
                    $event = new AccessPropertyEvent($className, $property);
                    $this->app['dispatcher']->dispatch('entities.access.property', $event);
                    
                    if ( $event->isAllowed() ) {
                        $result[$property] = $metaData->getReflectionProperty($property);
                    }
                }
            }            
        }
        else {
            foreach ( $metaData->getReflectionProperties() as $property ) {
                if ( strcmp( $property->getName(), $metaData->getSingleIdReflectionProperty()->getName() ) != 0 ) {
                    // Handle property access privilegies if any.
                    $event = new AccessPropertyEvent($className, $property->getName());
                    $this->app['dispatcher']->dispatch('entities.access.property', $event);
                    
                    if ( $event->isAllowed() ) {
                        $result[$property->getName()] = $metaData->getReflectionProperty($property->getName());
                    }
                }
            }
        }
        return $result;
    }
    
    /**
     * Gets instance primary key value.
     * @param mixed $instance
     */
    public function getId($instance) {
        $metaData = $this->app['db.orm.em']->getClassMetadata( get_class($instance) );
        
        $idProperty = $metaData->getSingleIdReflectionProperty();
        return $idProperty->getValue($instance);
    }
}

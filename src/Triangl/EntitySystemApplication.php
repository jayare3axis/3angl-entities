<?php

namespace Triangl;

/**
 * Application with security module.
 */
class EntitySystemApplication extends Application {
    
    /**
     * Overriden.
     */
    protected function init() {
        parent::init();
        
        $this->register( new EntitySystem() );
    }
}

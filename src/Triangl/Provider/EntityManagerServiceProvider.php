<?php

namespace Triangl\Provider;

use Silex\ServiceProviderInterface;

use Doctrine\Common\Cache\ArrayCache;
use Doctrine\ORM\Configuration as ORMConfiguration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\DriverChain;

use Knp\DoctrineBehaviors\ORM\Translatable\TranslatableSubscriber;
use Knp\DoctrineBehaviors\Reflection\ClassAnalyzer;

/**
 * Registers entity manager for object relation maping purposes.
 */
class EntityManagerServiceProvider implements ServiceProviderInterface {
    /**
     * Implemented.
     */
    public function register(\Silex\Application $app)
    {
        // Register Doctrine Entity Manager.        
        $app['db.orm.em'] = $app->share(function ($app) {
            $cache = new ArrayCache();
            $config = new ORMConfiguration;
            $config->setMetadataCacheImpl($cache);
            $config->setQueryCacheImpl($cache);
            $chain = new DriverChain();
            
            // Register entity namespaces.
            foreach ($app['db.orm.entities'] as $entity) {
                switch ($entity['type']) {
                    case 'default':
                    case 'annotation':
                        $driver = $config->newDefaultAnnotationDriver( (array)$entity['path'] );
                        $chain->addDriver($driver, $entity['namespace']);
                    break;
                    case 'yml':
                        $driver = new YamlDriver( (array)$entity['path'] );
                        $driver->setFileExtension('.yml');
                        $chain->addDriver($driver, $entity['namespace']);
                    break;
                    case 'xml':
                        $driver = new XmlDriver( (array)$entity['path'], $entity['namespace'] );
                        $driver->setFileExtension('.xml');
                        $chain->addDriver($driver, $entity['namespace']);
                    break;
                    default:
                        throw new \InvalidArgumentException( sprintf('"%s" is not a recognized driver', $entity['type']) );
                }
            }
            
            // Register entity filters.
            foreach ($app['db.orm.filters'] as $name => $val) {
                $config->addFilter($name, $val);
            }

            $config->setMetadataDriverImpl($chain);
            $config->setProxyDir($app["path"] . '/doctrine/Proxy');
            $config->setProxyNamespace('DoctrineProxy');
            $config->setAutoGenerateProxyClasses(true);

            $em = EntityManager::create($app['db'], $config);
            // TO - DO handle selected locale
            $em->getEventManager()->addEventSubscriber( new TranslatableSubscriber( new ClassAnalyzer(), function() { return "cs"; }, null, 
                'Knp\DoctrineBehaviors\Model\Translatable\Translatable', 'Knp\DoctrineBehaviors\Model\Translatable\Translation',
                "LAZY", "LAZY") );
            return $em;
        });
    }

    /**
     * Implemented.
     */
    public function boot(\Silex\Application $app)
    {
    }
}

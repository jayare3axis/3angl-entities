<?php

namespace Triangl;

use Silex\ServiceProviderInterface;

use Triangl\Provider\EntityManagerServiceProvider;

/*
 * Triangl entity system module.
 */
class EntitySystem implements ServiceProviderInterface {
    /**
     * Implemented.
     */
    public function boot(\Silex\Application $app) {        
    }

    /**
     * Implemented.
     */
    public function register(\Silex\Application $app) {
        $engine = $app["triangl.engine"];
        $engine->createDirIfNotExist($app["path"] . '/doctrine/Proxy');
        
        $app['db.orm.entities'] = array();
        $app['db.orm.filters'] = array();
        
        // Register services.
        $app['triangl.entities'] = $app->share( function ($app) {
            return new EntitySystemHelper($app);
        } );
        $app->register( new EntityManagerServiceProvider() );
    }
}

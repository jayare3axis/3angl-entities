<?php

namespace Triangl;

use Symfony\Component\EventDispatcher\Event;

/**
 * Event invoked before property of entity is accessed.
 * Use it to hide property before user for example due to
 * incufficient permissions.
 */
class AccessPropertyEvent extends Event {
    private $className;
    private $property;
    private $allow;
    
    /**
     * Default constructor.
     */
    public function __construct($className, $property) {
        $this->className = $className;
        $this->property = $property;
        $this->allow = true;
    }
    
    /**
     * Gets class name.
     * @return string
     */
    public function getClassName() {
        return $this->className;
    }
    
    /**
     * Gets property.
     * @return string
     */
    public function getProperty() {
        return $this->property;
    }
    
    /**
     * Sets result.
     * @param boolean $val
     * @return \Triangl\AccessPropertyEvent this
     */
    public function setAllowed($val = true) {
        $this->allow = $val;
        return $this;
    }
    
    /**
     * Gets result.
     * @return boolean
     */
    public function isAllowed() {
        return $this->allow;
    }
}
